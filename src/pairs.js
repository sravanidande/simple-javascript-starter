const pairs = arr => {
  const result = [];
  for (let i = 0; i < arr.length - 1; i += 1) {
    result.push(arr.slice(i, i + 2));
  }
  return result;
};

export default pairs;
