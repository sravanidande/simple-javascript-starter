import { range } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import repeatTwice from './repeatTwice';
import pairs from './pairs';
import squareOdds from './squareOdds';
import partition from './partition';
import secondMax from './secondMax';

range(1, 20)
  .pipe(
    filter(x => x % 2 === 1),
    map(x => x * x),
  )
  .subscribe(x => console.log(x));

console.log('helo');

console.log(repeatTwice([1, 2, 3, 4, 5, 6]));

console.log(pairs([1, 2, 3, 4, 5, 6]));

console.log(squareOdds([2, 3, 4, 5, 6]));

console.log(partition([1, 2, 3, 4, 5], 3));

console.log(secondMax([33, 54, 66, 23]));
