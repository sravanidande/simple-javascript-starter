const secondMax = arr => {
  let max = arr[0];
  let secMax = arr[0];
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] > max) {
      secMax = max;
      max = arr[i];
    }
  }
  return secMax;
};

export default secondMax;
